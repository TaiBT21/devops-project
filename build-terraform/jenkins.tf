resource "aws_instance" "jenkins-server" {
  ami = "ami-0ccd09798bcc264cd"
  instance_type = "t2.large"
  key_name = "server-keypair"
  tags = {
    Name = "jenkins-server"
  }
  subnet_id = module.vpc.myvpc.private_subnets[0]
  depends_on = [
    module.vpc
  ]
}
